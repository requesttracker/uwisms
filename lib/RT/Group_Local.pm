=head2 MemberNames

Returns an array of the names of all of this group's members


=cut

sub MemberNames {
    my $self = shift;
    return sort grep defined && length,
        map $_->Name,
        @{ $self->UserMembersObj->ItemsArrayRef };
}



=head2 MemberNamesAsString

Returns a comma delimited string of the names of all users 
who are members of this group.

=cut


sub MemberNamesAsString {
    my $self = shift;
    return (join(',', $self->MemberNames));
}

1;
