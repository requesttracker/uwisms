=head2 RequestorNames

B<Returns> String: All Ticket Requestor names as a string.

=cut

sub RequestorNames {
    my $self = shift;
    return $self->RoleNames('Requestor');
}


=head2 AdminCcNames

returns String: All Ticket AdminCc names as a string

=cut

sub AdminCcNames {
    my $self = shift;
    return $self->RoleNames('AdminCc');
}

=head2 CcNames

returns String: All Ticket Ccs as a string of names

=cut

sub CcNames {
    my $self = shift;
    return $self->RoleNames('Cc');
}

=head2 RoleNames

Takes a role name and returns a string of all the names (userids) for
users in that role

=cut

sub RoleNames {
    my $self = shift;
    my $role = shift;

    unless ( $self->CurrentUserHasRight('ShowTicket') ) {
        return undef;
    }
    return ( $self->RoleGroup($role)->MemberNamesAsString);
}

1;
