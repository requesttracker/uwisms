use 5.008003;
use strict;
use warnings;

package UWisms;

use RT::ACL;

our $VERSION = '0.02';

=head1 NAME

RTx::UWisms - Specific UI tweaks for University of Waterloo

=head1 DESCRIPTION

=head1 AUTHOR

Jeff Voskamp <javoskam@uwaterloo.ca>

=head1 LICENSE

Under the same terms as Perl itself.

=cut

'RT::System'->AddRight ( Staff => ShowRTIR => 'Show the RTIR menus' ); #loc

1;

